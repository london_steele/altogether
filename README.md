Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
    *  Difference between logging and log to console:
Logging allows you to see messages from program execution like log to console, but logging also puts the messages in a physical place, such as logger.log. This means that the messages will persist even after we close the console, because it is saved to a location in memory. This allows us to later retrieve and analyze the messages if we wish/need to.
    *  Difference between logger.log and the log to console:
    
	lines 3 and 4:
	2018-02-08 02:42:46 FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
	2018-02-08 02:42:46 FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
2.  Where does this line come from? 
	
		FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
TimeMe. At the end of the scope of this function call, it throws a TimeException, which extends Exception, and the logged information is from the super class of TimeException, which is Exception.
   
1.  What does Assertions.assertThrows do?
    *  Asserts that execution of the supplied executable throws an exception of the expectedType and returns the exception.
    		Assertions.assertTrue(Timer.timeMe(0) >= 0) makes sure that the Timer's returned time is a positive value (no overflow).
    
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
    2.  Why do we need to override constructors?
    3.  Why we did not override other Exception methods?	
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
    *  It makes sure that our logger gets configured correctly at execution. 
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
    *  Bitbucket Server uses Markdown for formatting text. Markdown can be used in any pull request’s descriptions or comments, or in README files (if they have the .md file extension, as we see here). Basically, it's a text formatting syntax that is/can be used in various Bitbucket text inputs.
  
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel)
![Alt text](/Users/london/Desktop/junit_run.tiff "JUnit ScreenShot") 
1.  Make a printScreen of your eclipse Maven test run, with console
![Alt text](/Users/london/Desktop/maven_test.tiff "Maven ScreenShot")
1.  What category of Exceptions is TimerException and what is NullPointerException
1.  Push the updated/fixed source code to your own repository.